<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\AdminInfo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admininfo = AdminInfo::create([
            'first_name' => 'Christian',
            'middle_name' => 'C',
            'last_name' => 'Lactawan',
            'gender' => 'Male',
            'contact_number' => '0912344578'
        ]);

        Admin::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('123123'),
            'admin_info_id' => $admininfo->id
        ]);
    }
}
