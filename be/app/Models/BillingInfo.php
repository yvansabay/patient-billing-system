<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillingInfo extends Model
{
    use HasFactory;

    protected $table = 'user_billing_infos';
    public $guarded = [];
    
    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
