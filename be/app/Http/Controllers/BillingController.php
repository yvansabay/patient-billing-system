<?php

namespace App\Http\Controllers;

use App\Models\BillingInfo;
use Illuminate\Http\Request;

class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return response()->json(BillingInfo::with(['user', 'user.info'])->paginate(10));
    }

    public function store(Request $request){
        $data = [
            'user_id' => $request->user_id,
            'name' => $request->name,
            'payment_fee' => $request->payment_fee,
            'remarks' => $request->remarks,
            'status' => $request->status,
        ];

        $billing = BillingInfo::create($data);
        $added_billing = BillingInfo::with(['user', 'user.info'])->where('id', $billing->id)->first();
        return response()->json(['msg' => 'Billing added successfully!', 'billing' => $added_billing], 200);
    }

    public function update(Request $request, $id){
        $billinginfo = BillingInfo::where('id', $id)->first();
        if($billinginfo){
            $billinginfo->update([
                'name' => $request->name,
                'payment_fee' => $request->payment_fee,
                'remarks' => $request->remarks,
                'status' => $request->status,
            ]);
        }
        else {
            return response()->json(['msg'=>'Billing Info not found'], 404);
        }

        return response()->json(['msg'=>'Billing Info Updated successfully'], 200);
    }

    public function search(Request $request){
        $billing = BillingInfo::whereHas('user.info', function ($query) {
            $query->where('first_name', 'like', '%'.request()->get('search').'%')
            ->orWhere('middle_name', 'like', '%'.request()->get('search').'%')
            ->orWhere('last_name', 'like', '%'.request()->get('search').'%');
        })->with(['user','user.info'])->paginate(10);

        return response()->json($billing, 200);

    }
    public function destroy(Request $request, $id){
        BillingInfo::destroy($id);
        return response()->json(['msg' => 'Billing deleted successfully'], 200);
    }

}
