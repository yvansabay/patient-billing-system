import API from '../../base/'

export default {
  namespaced: true,
  state: {
   appointments: [],
   summary: [],
   billing: {},
  },
  getters: {
    GET_APPOINTMENTS(state) {
      return state.appointments;
    }
  },
  mutations: {
    SET_APPOINTMENTS(state, data) {
      state.appointments = data
    },
    SET_SUMMARY(state, data) {
      state.summary = data
    },
    SET_BILLING(state, data) {
     state.billing = data
   },
  },
  actions: {
    async getAppointments({commit}){
     const res = await API.get('/user/appointment').then(res => {
      commit('SET_APPOINTMENTS', res.data)
     }).catch(err => {
      return err.response;
     })

     return res;
    },
    async getSummary({commit}){
      const res = await API.get('/user/summary').then(res => {
        commit('SET_SUMMARY', res.data)
       }).catch(err => {
        return err.response;
       })
  
       return res;
    },
    async getBillings({commit}, page){
     const res = await API.get(`/user/billing?page=${page}`).then(res => {
       commit('SET_BILLING', res.data)

       return res;
     }).catch(err => {
      return err.response;
     })

     return res;
   },
   async searchBilling({commit}, {data, page}){
     const res = await API.post(`/user/search/billing?page=${page}`, data).then(res => {
       commit('SET_BILLING', res.data)

       return res;
     }).catch(err => {
      return err.response;
     })

     return res;
   },
  },
}